<!DOCTYPE html>
<head>
<title>Calculator</title>
<meta charset="utf-8"/>

<style>
body {
background-color: #FFD6D6;
font-family: Georgia;
font-size: 20pt;
line-height: 2;
position: fixed;
margin: 30px;
}
</style>
</head>

<body>
<?php
if(!isset($_GET["operation"]) {
print "Please choose operation.";
}

else {
if($_GET["operation"] == "add") {
$result = $_GET["a"] + $_GET["b"];
print "The answer is ".$result;
}

else if($_GET["operation"] == "subtract") {
$result = $_GET["a"] - $_GET["b"];
print "The answer is ".$result;
}

else if($_GET["operation"] == "divide") {
if($_GET["b"] == 0) {
$result = "undefined: you cannot divide by 0."; }
else {
$result = $_GET["a"] / $_GET["b"]; }
print "The answer is ".$result;
}

else if($_GET["operation"] == "multiply") {
$result = $_GET["a"] * $_GET["b"];
print "The answer is ".$result;
}
}

?> 


</body>
</html>